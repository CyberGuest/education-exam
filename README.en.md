# v3学习记录
## 自我介绍：
```
后端开发者，初学Vue
background: linear-gradient(to right,#ff6034,#ee0a24);
```
### 学习记录和碰到的问题以及思考
day1: v3生命周期- 部分方法不常用也不知道怎么使用待学习🤓
```
1 - 开始创建组件 - setup();
2 - 组件挂载到页面之前执行 - onBeforeMount();
3 - 组件挂载到页面之后执行 - onMounted();
4 - 组件更新之前 - onBeforeUpdate();
5 - 组件更新之后 - onUpdated();
6 - 组件卸载之前执行的函数 - onBeforeUnmount();
7 - 组件卸载完成后执行的函数 - onUnmounted();
```
day2: v3路由刷新
```
<template>
  <!-- vue3.0配置 -->
  <router-view v-slot="{ Component }">
    <keep-alive>
      <component :is="Component"  v-if="$route.meta.keepAlive"/>
    </keep-alive>
    <component :is="Component"  v-if="!$route.meta.keepAlive"/>
  </router-view>
</template>

页面部分刷新
被keepAlive包裹的组件和页面，页面进入时执行的生命周期为：created->mounted->activated;
其中created->mounted是页面第一次进入才会执行，activated生命周期在页面每次进入都会执行，特属于keepAlive的一个生命周期，所以我们把页面每次进来要进行的操作放入该生命周期即可
activated() {
    // 页面每次进入清空输入框
   this.$refs.don.inputValue = '';
}
```

### Problems
```
1. 刷新页面报错：Uncaught SyntaxError: Unexpected token <
   解决 -> 在vue.config.js中加入 publicPath: '/',
2. 父子组件传值 父组件通过click事件赋值 子组件不刷新
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).


