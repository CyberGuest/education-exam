# 在线考试系统

#### :rice: 介绍
基于Vue3.x搭建的H5在线考试系统部署在公众号
目前只实现了章节测试功能，在线课程播放视频，微信支付，每天迭代新功能
后端后续开源因为包含敏感信息
本人后端开发者 未来可期

#### :rice: 软件架构
Vue3.x+Vue-router4.x+ Vux4.x+vant

#### :rice: 安装教程
1.  安装Vue环境 node.js
2.  导入项目依赖 npm install
3.  启动项目 npm run serve

#### :rice: 使用说明
暂无

#### :rice: 内容展示
<p align="center">
  <img src="https://images.gitee.com/uploads/images/2021/0714/172834_658ce3ef_7555104.jpeg" width="20%" />
  <img src="https://images.gitee.com/uploads/images/2021/0714/174753_2379fef1_7555104.jpeg" width="20%" />
  <img src="https://images.gitee.com/uploads/images/2021/0714/174731_86d55d97_7555104.jpeg" width="20%" />
  <img src="https://images.gitee.com/uploads/images/2021/0714/174721_d900e2b9_7555104.jpeg" width="20%" />
</p>
#### :rice: 微信扫码体验
<div width="100%">
https://oasisplan.cn
</div>

