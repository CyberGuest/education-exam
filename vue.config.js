const path = require("path");
const resolve = dir => path.join(__dirname, dir);
module.exports = {
  publicPath: process.env.NODE_ENV === 'production' ? '/' : '/',
  productionSourceMap: false,
  outputDir: 'dist',  //build输出目录
  assetsDir: 'assets', //静态资源目录（js, css, img）
  lintOnSave: false, //是否开启eslint
  devServer: {
    open: true, // 是否打开浏览器
    host: "0.0.0.0",
    port: "8080", // 端口号默认8080
    // https: true,  //是否使用https协议
    // hotOnly: true, // 热更新
    overlay: { // 让浏览器 overlay 同时显示警告和错误
      warnings: true,
      errors: true
    },
    proxy: {
      [process.env.VUE_APP_API]: {
        // target: 'http://localhost:8081/api',
        target: process.env.VUE_APP_API_HOST, // 目标代理接口地址
        secure: false,
        autoRewrite: true,
        changeOrigin: false, //是否跨域
        // ws: false, // 是否启用websockets
        pathRewrite: {
          ['^' + process.env.VUE_APP_API]: ''
        }
      }
    },
  },
  // 调整内部的 webpack 配置。
  chainWebpack: config => {
    config.resolve.alias
      .set("@", resolve("src"));
  },
};
