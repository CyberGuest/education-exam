//自定义组件
// import ZhengDatePicker from "@/components/zheng-date-picker";

import { createApp } from "vue"
import App from './App.vue'
//router vuex
import router from './router'
import store from './store'

//vant
import Vant from "vant";
import 'vant/lib/index.css';
import { showToast,showSuccessToast, showFailToast  } from "vant"
// import { ConfigProvider } from "vant";

//icon
import "@/assets/iconfont/iconfont.css";
// import { Toast, Dialog } from "vant"
//视频播放器
import Vue3VideoPlayer from '@cloudgeek/vue3-video-player'
import '@cloudgeek/vue3-video-player/dist/vue3-video-player.css'

const app = createApp(App);
// app.config.productionTip = false;
//全局挂载
// app.config.globalProperties.$http=http;
// app.config.globalProperties.$toast=Toast;
// app.config.globalProperties.$dialog=Dialog;

app.use(Vant)
// app.use(Toast)
// app.use(ConfigProvider)
app.use(router)
app.use(store)
app.use(Vue3VideoPlayer, {
  lang: 'zh-CN'
})
app.mount('#app')
