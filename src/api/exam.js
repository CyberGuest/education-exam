import http from "@/utils/http";

/**
 * 试题列表
 * @param params
 * @returns {*}
 */
export function getQuestionList(params) {
  return http.get('/exam/question/list',params)
}

/**
 * 交卷
 * @param params
 * @returns {*}
 */
export function finishExam(params) {
  return http.post('/exam/record/finish',params)
}

/**
 * 考试记录
 * @param params
 * @returns {*}
 */
export function examRecord(params) {
  return http.get('/exam/record/list',params)
}

