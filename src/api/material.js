import http from "@/utils/http";

/**
 * 知识库列表
 * @param params
 * @returns {*}
 */
export function getMaterialList(params) {
  return http.get('/material/list',params)
}
