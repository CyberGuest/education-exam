import http from "@/utils/http";

/**
 * 获取类别树
 * @param dictDataId 字典数据ID
 * @returns {*}
 */
export function getCategoryTree(dictDataId) {
  return http.get('/education/categoryTree/'+dictDataId)
}
