import http from "@/utils/http";

/**
 * 注册
 */
export function register(dataForm){
  return http.post('/register', dataForm);
}

/**
 * 普通登录
 */
export function login(dataForm) {
  return http.post('/login', dataForm);
}

/**
 * 退出
 */
export function logout(){
  return http.post('/logout');
}

/**
 * 微信授权链接
 */
export function wxAuthUrl(state){
  return http.get('/wechat/public/authUrl/'+state);

}

/**
 * 微信授权登录
 */
export function wxAuthLogin(params) {
  return http.get('/wechat/public/authLogin', params);
}

/**
 * 获取用户信息
 */
export function getUserInfo() {
  return http.get('/@alias member/userInfo');
}

/**
 * 获取用户ID
 */
export function getUserId() {
  return http.get('/member/userId');
}

/**
 * 忽略Token验证测试
 */
export function notToken() {
  return http.get('/member/notToken');
}
