import http from "@/utils/http";

/**
 * 课程列表
 * @returns {*}
 */
export function getCourseList() {
  return http.get('/course/list')
}

/**
 * 课程信息
 * @param id
 * @returns {*}
 */
export function getCourseInfo(id) {
  return http.get('/course/'+id)
}

/**
 * 课程目录信息
 * @param params
 * @returns {*}
 */
export function getCourseCatalogueList(params) {
  return http.get('/course/catalogue',params)
}
