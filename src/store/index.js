import { createStore } from "vuex"
import http from "../utils/http";

export default createStore({
  state: {
    subjectSetting: {
      subjectId: '',
      subjectName: '请选择科目'
    }
  },
  modules: {
    // user
  },

  mutations: {
    // 重置vuex本地储存状态
    updateState(state, payload) {
         console.log(state, payload)
      Object.keys(payload).forEach((key) => {
        state[key] = payload[key];
      });
    }
  },

  actions: {
    updateState(ctx, { payload }) {
      console.log(ctx, {payload})
      ctx.commit("updateState", payload);
    },
    initApp(ctx) {
      // if (this.state.dictData.length <=0){
      //   return Promise.all([
      //     http.get('/education/dict/data/list',{params: { dictType: this.state.dictTypeForSubject}}) //加载字典
      //   ]).then(([dictData]) => {
      //     ctx.commit("updateState", {
      //       dictData: dictData.data.data || []
      //     });
      //   });
      // }
    },
  }
})
